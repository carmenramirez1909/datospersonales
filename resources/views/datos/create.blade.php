@extends('layouts.app')

@section('content')
<form action="{{route('datos.store')}}" method="post">
@csrf

<div class="container">

<div class="form-group">
<input type="text" class="form-control" name="nombre" placeholder="Nombre">
</div>
<div class="form-group">
<input type="text" class="form-control" name="apellidopaterno" placeholder="Apellido Paterno">
</div>
<div class="form-group">
<input type="text" class="form-control" name="apellidomaterno" placeholder="Apellido Materno">
</div>
<div class="form-group">
<input type="date" class="form-control" name="fecha" placeholder="Fecha de nacimiento">
</div>
<button type="submit" class="btn btn-warning">Guardar Registro</button>
</div>
</form>



@endsection