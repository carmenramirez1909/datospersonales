@extends('layouts.app')

@section('content')
<body background-color:#FF0000;>
    

<div class="text-center">
<h1>Datos Personales</h1>
<div class="container ">
<table class="table table-bordered">
<thead class="thead-dark">

<tr>
<th>ID</th>
<th>NOMBRE</th>
<th>APELLIDO PATERNO</th>
<th>APELLIDO MATERNO</th>
<th>FECHA DE NACIMIENTO</th>
<th>ACCION</th>


</tr>

</thead>
<tbody>
@foreach($datos as $dato)
<tr>
<td>{{$dato->id}}</td>
<td>{{$dato->nombre}}</td>
<td>{{$dato->apellidopaterno}}</td>
<td>{{$dato->apellidomaterno}}</td>
<td>{{$dato->fecha}}</td>
<td>
 <a href="{{url('/datos/'.$dato->id.'/edit')}}" class="btn btn-link">ACTUALIZAR</a>

</td>
</tr>
@endforeach

</tbody>


</table>

<a href="{{url('/datos/create')}}" class="btn btn-warning">Agregar nuevo registro</a>

</div>


</div>

</body>

@endsection