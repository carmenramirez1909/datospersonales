@extends('layouts.app')

@section('content')


<form action="{{route("datos.update", $dato ->id)}}" method="POST">
{{method_field('PATCH')}}
@csrf

<div class="container">

<div class="form-group">
<input type="text" class="form-control" name="nombre" value="{{$dato->nombre}}" placeholder="Nombre">
</div>
<div class="form-group">
<input type="text" class="form-control" name="apellidopaterno" value="{{$dato->apellidopaterno}}" placeholder="Apellido Paterno">
</div>
<div class="form-group">
<input type="text" class="form-control" name="apellidomaterno" value="{{$dato->apellidopaterno}}" placeholder="Apellido Materno">
</div>
<div class="form-group">
<input type="date" class="form-control" name="fecha"  value="{{$dato->fecha}}" placeholder="Fecha de nacimiento">
</div>
<button type="submit" class="btn btn-warning">Guardar Registro</button>
</div>
</form>


@endsection